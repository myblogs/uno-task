<?php
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

Route::post("register", [AuthController::class, "register"])->name("register");
Route::get("register", [AuthController::class, "registerView"])->name("register-view");
Route::get("login", [AuthController::class, "loginView"])->name("login-view");
Route::post("login", [AuthController::class, "login"])->name("login");
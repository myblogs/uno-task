<?php
use App\Http\Controllers\Home\HomeController;
use Illuminate\Support\Facades\Route;

Route::get("home",[HomeController::class,"home"])->name("home");
Route::get("logout", [HomeController::class, "logout"])->name("logout");

<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
Route::prefix("products")->group(function(){
    Route::get("show-cart",[ProductController::class,"showCart"]);
    Route::get("{product}",[ProductController::class,"showProduct"])->name("show-product");
    Route::post("add-to-cart",[ProductController::class,"addToCart"])->name("add-to-cart");
});


    @extends('layouts.layout')
    @section('content')
    <!-- hafe -->
    <section>
        <div class="container">
            <div class="row">
                <h4 class="text-center" id="uuu">Product details
                </h4>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="pain12">
                        <h5 class="text-right" id="hhj">Name : {{$product->name}}</h5>
                        <h5 class="text-right" id="hhj"> Salary : {{$product->price}}</h5>
                        <h5 class="" id="hhj"> Discription :
                            {{$product->description}}
                        </h5>


                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <img src="../{{$product->image}}" class="opp" alt="">
                </div>

            </div>
            <div class="row4 row">
                <form method="POST" action="{{route('add-to-cart')}}">
                    @csrf
                    <input name="product_id" type="hidden" value="{{$product->id}}" />
                    <div class="col-lg-6">
                        <input name="quantity" type="number" class="form-control">
                    </div>
                    <div class="col-lg-6">
                        <button type="submit" class="btn btn-success" id="qqmn">Add To Cart</button>
                    </div>
                </form>
            </div>
        </div>
        </div>

    </section>
    @push('details-css')
    <link rel="stylesheet" href="../css/detailes.css">
    @endpush
    @endsection

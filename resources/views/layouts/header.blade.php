<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    @stack('home-css')
    @stack('register-css')
    @stack('login-css')
    @stack('details-css')
    <script src="../jquery-3.6.0.min.js"></script>
</head>
<body>




    <!-- head english -->
    <section class="section1" id="enhead">
        <section class="rwe">
            <div class="row1 row">
                @guest
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 hidden-xs" id="col1">
                    <div class="btn btn-primary"><a href="{{route('register-view')}}" class="ad">Register</a></div>
                    <span class="po">/</span>
                    <div class="btn btn-primary"><a href="{{route('login-view')}}" id="nasd"> Log in </a></div>
                </div>
                @endguest
                @auth
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 hidden-xs" id="col1">
                    <div class="btn btn-primary"><a href="{{route('logout')}}" class="ad">Logout</a></div>
                </div>
                @endauth
                
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 visible-xs" id="col1">
                    <div class="btn btn-primary" id="yu"><span class="glyphicon glyphicon-tasks"></span></div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 hidden-xs">
                    <div class="input-group">
                        <span class="rtt input-group-addon">
                            <span class="glyphicon glyphicon-search"></span>
                        </span>
                        <input type="text" placeholder="Search" class="inpp form-control" aria-label="...">
                    </div>

                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                    <img src="../images/ono.jpg" class="img1" alt="">
                </div>

            </div>
        </section>
        <div class="container-fluid">

            <div class="row1 row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                    <img src="../images/ono.jpg" class="img1" alt="">
                </div>

                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 hidden-xs">
                    <div class="input-group">
                        <span class="rtt input-group-addon">
                            <span class="glyphicon glyphicon-search"></span>
                        </span>
                        <input type="text" placeholder="بحث" class="inpp form-control" aria-label="...">
                    </div>


                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 hidden-xs" id="col1">
                    <div class="btn btn-primary"><a href="../onoo/login.html">التسجيل</a></div>
                    <span class="po">/</span>
                    <div class="btn btn-primary"> <a href="./Register.html">تسجيل الدخول </a> </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 visible-xs" id="col1">
                    <div class="btn btn-primary">wre</div>

                </div>
            </div>



            <div class="row">
                <h5 class="text-center" id="qwe">Buy and sell from anywhere for free
                </h5>
            </div>

            <div class="row text-center">



            </div>

        </div>

        <div class="oopz">

            <div class="input-group">
                <span class="rtt input-group-addon">
                    <span class="glyphicon glyphicon-search"></span>
                </span>
                <input type="text" placeholder="Search" class="inpp form-control" aria-label="...">
            </div>
            <div class="yyyy">
                <div class="btn btn-primary"><a href="" style="color:white">Register</a></div>
                <span class="po">/</span>
                <div class="btn btn-primary"> <a href="" style="color:white">Log in </a> </div>
            </div>
        </div>

    </section>

    <!-- head english -->

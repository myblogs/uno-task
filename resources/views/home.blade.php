@extends("layouts.layout")
@section('title', 'Home Page')
@section('content')
<!-- hafe english -->
<section id="hafar">
    <div class="container">
        <div class="ri row text-center">
            <h3 class="trty">Latest products
            </h3>
            @foreach($products as $product)
            <div class="product col-lg-3 col-md-3 col-sm-4 col-xs-12">
                <a href="./detailes.html" id="typ">
                    <div class="cart">
                        <div>
                            <img src="../{{$product->image}}" class="img-responsive" class="card-img-top" id="img7" alt="...">
                        </div>
                    </div>
                </a>
                <h4 class="text-center" id="tt">{{$product->name}} </h4>
                <h4 class="text-center" id="tt">{{$product->price}}</h4>
                <a href="{{route('show-product',['product'=>$product->id])}}" class="btn btn-success">Details</a>
            </div>
            @endforeach
        </div>
        <div class="ri2 row text-center">
            <h3 class="trty text-center">
                Categories
            </h3>
            
            <table class="table table-bordered" style="direction: ltr;">
                <tr>
                    <td>Number</td>
                    <td>Name</td>
                </tr>
                @foreach($categories as $index=>$category)
                <tr>
                    <td>{{$index+1}}</td>
                    <td>{{$category->name}}</td>
                </tr>
                @endforeach
            </table>

        </div>
        <div class="ri3 row text-center">
            <h3 class="trty">Best Stores
            </h3>
            <table class="table table-bordered" style="direction: ltr;">
                <tr>
                    <td>Number</td>
                    <td>Name</td>
                </tr>
                @foreach($stores as $index=>$store)
                <tr>
                    <td>{{$index+1}}</td>
                    <td>{{$store->name}}</td>
                </tr>
                @endforeach
            </table>



        </div>

    </div>
</section>
<!-- hafe english -->
@push("home-css")
<link rel="stylesheet" href="../css/home-style.css">
@endpush
@push("home-js")
<script src="../js/Home.js"></script>
@endpush
@endsection

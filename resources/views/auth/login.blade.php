     @extends("layouts.layout")
     @section('content')
     <div class="container">
         <div class="row">
             <div class="col-lg-12 col-md-12 col-sm-12">
                 <center>
                     <div class="show">
                         @if(Session::has("error"))
                         <div class="alert alert-danger" id="hjk">
                            <p class="por"> {{Session::get("error")}}</p>
                         </div>
                         @endif
                         <div class="paint">
                             <h4 class="t1 text-center">Enter the data
                             </h4>
                             <form method="POST" action="{{route('login')}}">
                                 <div class="move">
                                     @csrf
                                     <input name="email" type="text" id="in1" class="er form-control" placeholder="Email  " style="text-align:left" />
                                     <input name="password" type="text" id="in2" class="er form-control" placeholder="Password " style="text-align:left" />
                                     <div class="mo2"><span id="sds2" style="color:white;margin-left:39%;position: relative;opacity:1; bottom: 17px;"> <span class="qw12">Policy and terms
                                             </span></span></div><input type="checkbox" name="" id="qw1">
                                     <button type="submit" class="sd btn btn-success">
                                         login
                                     </button><br />
                                     <a href="#" class="fd">Forget Password</a>
                                 </div>
                             </form>
                         </div>
                 </center>
             </div>
         </div>
     </div>

     @push('login-css')
     <link rel="stylesheet" href="../css/register.css">
     @endpush
     @push('login-js')
     <script src="../js/register.js"></script>
     @endpush
     @endsection

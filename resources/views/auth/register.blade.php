    @extends("layouts.layout")
    @section('content')
    <!-- hafe english -->
    <div class="container" id="ena">
        <div class="row">
            <div class="col-lg-6 col-lg-push-3 col-md-6 col-md-push-3 col-sm-12 col-xs-12">
                <center>
                    <div class="show">
                        <div class="paint">
                            <h4 class="t1 text-center"><bdi class="t11">Create a new account in onoo
                            
                            </h4>
                            <h6 class="t2HG text-right">Enter the data
                            </h6>
                            <form method="POST" action="{{route('register')}}">
                                @csrf
                                <div class="move">
                                    <input name="first_name" type="text" id="in1" class="er form-control" placeholder="First Name " style="text-align:left" />
                                    @error("first_name")
                                    <div class="text-danger">
                                        {{$message}}
                                    </div>
                                    @enderror
                                    <input name="last_name" type="text" id="in2" class="er form-control" placeholder="Last Name" style="text-align:left" />
                                    @error("last_name")
                                    <div class="text-danger">
                                        {{$message}}
                                    </div>
                                    @enderror
                                    <input name="email" type="text" id="in3" class="er form-control" placeholder="Email" style="text-align:left" />
                                    @error("email")
                                    <div class="text-danger">
                                        {{$message}}
                                    </div>
                                    @enderror
                                    
                                    <input name="phone" id="in4" class="er form-control" placeholder="Phone Number " style="text-align:left" />
                                    @error("phone")
                                    <div class="text-danger">
                                        {{$message}}
                                    </div>
                                    @enderror
                                    
                                    <input name="password" id="in5" class="er form-control" placeholder="Password " style="text-align:left" />
                                    @error("password")
                                    <div class="text-danger">
                                        {{$message}}
                                    </div>
                                    @enderror
                                    <div class="mo1"><span id="sds" style="color:white;margin-left:35%;opacity:1;"><span class="ddddssssss">Policies and privacy
                                            </span> </span></div><input type="checkbox" name="" id="qwas">
                                    <div class="mo2"><span id="sds2q" style="color:white;margin-right:39%;position: relative;opacity:1; bottom: 15px;"> <span class="qw122"> Policies and terms
                                            </span></span></div><input type="checkbox" name="" id="qw1as">
                                    <button type="submit" class="sd btn btn-success">Register</button><br />
                                    <a href="{{route('login-view')}}" class="fddd">I have an account I want to log in
                                    </a>
                            </form>
                        </div>
                        <br>
                        <br>
                       
                    </div>
                </center>
            </div>
        </div>
    </div>
    <!-- hafe  english-->
    @push('register-css')
    <link rel="stylesheet" href="../css/login.css">
    @endpush
    @push('register-js')
    <script src="../js/login.js"></script>
    @endpush
    @endsection

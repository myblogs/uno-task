<?php
namespace App\Repositories;

use App\Models\Product;
use App\Models\Category;
use App\Models\Store;
class HomeRepository
{
    public function getLatestProducts($length)
    {
        return Product::take($length)->latest()->get();
    }

    public function getCategories(){
        return Category::get();
    }

    public function getStores(){
        return Store::get();
    }
    
}

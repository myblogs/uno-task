<?php

namespace App\Repositories;

use App\Models\User;

class AuthRepository
{

    public function create($userData)
    {
        return User::create($userData);
    }
}

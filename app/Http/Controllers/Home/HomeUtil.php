<?php
namespace App\Http\Controllers\Home;
trait HomeUtil
{
    
    protected function getLatestProducts()
    {
        return $this->homeRepository->getLatestProducts(4);
    }

    protected function getCategories()
    {
        return $this->homeRepository->getCategories();
    }

    protected function getStores()
    {
        return $this->homeRepository->getStores();
    }
}

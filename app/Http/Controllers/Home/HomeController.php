<?php

namespace App\Http\Controllers\Home;
use App\Repositories\HomeRepository;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller
{
    use HomeUtil;
    private $homeRepository;
    function __construct(HomeRepository $homeRepository){
        $this->homeRepository=$homeRepository;
        $this->middleware("auth");
    }

    public function home(){
        return view("home")
        ->with("products",$this->getLatestProducts())
        ->with("categories",$this->getCategories())
        ->with("stores",$this->getStores());
    }
     
    public function logout(){
        Auth::logout();
        return redirect()->route("login-view");
    }
}

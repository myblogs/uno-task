<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Repositories\AuthRepository;
use Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    private $authRepository;
    public function __construct(AuthRepository $authRepository)
    {
        $this->authRepository = $authRepository;
        $this->middleware("guest");
    }

    public function registerView()
    {
        return view("auth.register");
    }

    public function register(RegisterRequest $request)
    {
        $this->authRepository->create($request->input());
        return $this->login($request);
    }

    public function loginView()
    {
        return view("auth.login");
    }

    public function login()
    {
        if (auth()->attempt(['email' => request('email'), 'password' => request('password')])) {
            return redirect()->route("home");
        } else {
            session()->flash('error', "Error in login information");
            return redirect()->route("login-view");
        }
    }

}

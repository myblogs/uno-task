<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
        $this->middleware("auth");
    }

    public function showProduct(Product $product)
    {
        return view("product")
        ->with("product",$product);
    }

    public function addToCart(Request $request)
    {
        $this->productRepository->addToCart($request);
        return back();
    }

    public function showCart()
    {
        return $this->productRepository->showCart();
    }

}


window.onscroll=function(){
    onScrollFunction();
};

window.onresize=function(){
    onResizeFunction();
};

$(function(){
    onScrollFunction();
    onResizeFunction();
});

function onScrollFunction()
{
    let scroll=window.scrollY;
    if(scroll>85)
    {
        $('#baner-row').css('background-color','#173581');
    
    }else
    {
        $('#baner-row').css('background-color','transparent');
    }
 
}

function onResizeFunction()
{
    let w=window.innerWidth;
    if(w<= 767)
    {
        
        $('#menu-sinup').empty();
        $('#menu-sinup').append( $('#sin-cont').html());

        $('#search-contain').empty();
        $('#search-contain').append( $('#aside').html());
        
        $('#search-menu').show();
    } else
    {
        $('#search-menu').hide();
    }
    $('#menu-sinup').hide();
}

function OpenMenu(ele,target)
{
 $(ele).children('SPAN:last').toggleClass('glyphicon-chevron-up');
 $(ele).children('SPAN:last').toggleClass('glyphicon-chevron-down');
 $('#'+target).toggle(250);
}


function toggleMenu()
{
    $('#search-menu').toggleClass('open');
}
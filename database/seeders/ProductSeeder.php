<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::insert([
            [
                "name"=>"50 L DIGITAL ELECTRIC WATER",
                "price"=>2060,
                "description"=>"Burner. To adapt it to different gases, it is sufficient to exchange the injectors. (With stainless steel heads specially designed for the combustion of butane/propane and natural gases).",
                "capacitance"=>"50L",
                "brand"=>"Olympic",
                "power_source"=>"Electric",
                "warranty"=>"5",
                "image"=>"images/prod_1.jpg"
            ],
            [
                "name"=>"70 L FRESH ELECTRIC WATER",
                "price"=>1760,
                "description"=>"Burner. To adapt it to different gases, it is sufficient to exchange the injectors. (With stainless steel heads specially designed for the combustion of butane/propane and natural gases).",
                "capacitance"=>"70L",
                "brand"=>"Fresh",
                "power_source"=>"Electric",
                "warranty"=>"10",
                "image"=>"images/prod_2.webp"
            ],
            [
                "name"=>"50 L UNION AIR ELECTRIC WATER",
                "price"=>1760,
                "description"=>"Burner. To adapt it to different gases, it is sufficient to exchange the injectors. (With stainless steel heads specially designed for the combustion of butane/propane and natural gases).",
                "capacitance"=>"50L",
                "brand"=>"Union Air",
                "power_source"=>"Electric",
                "warranty"=>"3",
                "image"=>"images/prod_3.jpeg"
            ],
            [
                "name"=>"60 L DIGITAL ELECTRIC WATER",
                "price"=>1760,
                "description"=>"Burner. To adapt it to different gases, it is sufficient to exchange the injectors. (With stainless steel heads specially designed for the combustion of butane/propane and natural gases).",
                "capacitance"=>"40L",
                "brand"=>"Fresh",
                "power_source"=>"Electric",
                "warranty"=>"3",
                "image"=>"images/prod_4.webp"
            ],
            [
                "name"=>"60 L UNIVERSAL ELECTRIC WATER",
                "price"=>3000,
                "description"=>"Burner. To adapt it to different gases, it is sufficient to exchange the injectors. (With stainless steel heads specially designed for the combustion of butane/propane and natural gases).",
                "capacitance"=>"60L",
                "brand"=>"UNIVERSAL",
                "power_source"=>"Electric",
                "warranty"=>"10",
                "image"=>"images/prod_5.png"
            ],
        ]);
    }
}
